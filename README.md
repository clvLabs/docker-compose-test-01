# docker-compose-test-01

A **very basic** example on some Docker features.

It consists of three individual containers:

* `redis`: A Redis server
* `broadcaster`: A program which broadcasts the current time thru a Redis publication
* `listener`: A program which subscribes to the Redis publication

### redis
The redis container is the `Redis` image as it's found in the [Docker Hub](https://hub.docker.com/_/redis/), so no specific files are created for it.

### broadcaster / listener
These containers are created from a `Dockerfile` and each has its own folder.

The folder structure is as follows:
```
app
├── ctr
│   ├── entrypoint.sh
│   └── src
│       └── app.py
└── Dockerfile
```

* The `Dockerfile` is stored on the app root and defines how each container will be built.
* The `ctr` folder has the content that will be copied into the container.
* `ctr/entrypoint.sh` is an initialization script for each container.
* `ctr/src/app.py` is the Python code for the app.

## Development and production scenarios

This example includes two `compose` files: the *standard* `docker-compose.yml` and an additional `docker-compose-prod.yml`.

This can help development in a variety of ways:

* Different environment variables can be set for each scenario
* Development version can use the host's `ctr` folder instead of copying it into the container
* Development version can expose Redis' port to the host computer

## Usage

**NOTE**: ALL of these commands **MUST** be run inside the project folder!

### Start the development version:
```bash
$ docker-compose up
```

### Start the production version:
```bash
$ docker-compose -f docker-compose-prod.yml up
```

### Stop all the containers:

If you press `CTRL-C` while docker-compose is running, it will try to gracefully shut down all the containers, but it won't delete them. It's always better to do:
```bash
$ docker-compose down
```

### Force a rebuild before starting: add `--build`
```bash
$ docker-compose up --build
$ docker-compose -f docker-compose-prod.yml up --build
```

### Start in *detached* mode: add `-d`
```bash
$ docker-compose up -d
$ docker-compose -f docker-compose-prod.yml up -d
```

### View logs
```bash
$ docker-compose logs -f
```
