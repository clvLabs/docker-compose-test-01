# -*- coding: utf-8 -*-
#ª/usr/bin/python3

import os
import time
import redis

# Read environment variables
REDIS_HOST = os.environ['REDIS_HOST']
REDIS_PORT = int(os.environ['REDIS_PORT'])
SLEEP_TIME = int(os.environ['SLEEP_TIME'])
PUBLICATION = os.environ['PUBLICATION']
DEBUG = int(os.environ['DEBUG'])

# Connect to Redis
print('Opening Redis connection @ {:}:{:}...'.format(REDIS_HOST, REDIS_PORT))
r = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)

# Publish timestamp messages
print('Publishing to the {:} publication every {:} seconds...'.format(PUBLICATION, SLEEP_TIME))

while True:
    timeStr = time.strftime('%H:%M:%S', time.localtime())
    if DEBUG:
        print('Publishing current time: {:}'.format(timeStr))
    r.publish(PUBLICATION, timeStr)
    time.sleep(SLEEP_TIME)
