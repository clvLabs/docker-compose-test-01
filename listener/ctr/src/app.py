# -*- coding: utf-8 -*-
#ª/usr/bin/python3

import os
import time
import redis
import pprint

# Read environment variables
REDIS_HOST = os.environ['REDIS_HOST']
REDIS_PORT = int(os.environ['REDIS_PORT'])
PUBLICATION = os.environ['PUBLICATION']
DEBUG = int(os.environ['DEBUG'])

# Connect to Redis
print('Opening Redis connection @ {:}:{:}...'.format(REDIS_HOST, REDIS_PORT))
r = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)

# Subscribe to Redis publication
print('Subscribing to the {:} publication...'.format(PUBLICATION))
p = r.pubsub()
p.subscribe(PUBLICATION)

while True:
    m = None
    while not m:
        m = p.get_message()

    if DEBUG:
        print('Received new data: {:}'.format(pprint.pformat(m, indent=0, compact=True)))
    else:
        print(m['data'])
